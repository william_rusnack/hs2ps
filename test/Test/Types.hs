module Test.Types where

data Data1C2T = Data1C2Tc Int String

data ABC = A | B | C

newtype NT = NTc Int

data SumType a b c = SL a | SM b | SR c

data NonPrim = NonPrimC (Char, Either Int Double) (Either Bool String)

data Record = RecordC {a :: Int, b :: Bool, c :: Char}

newtype NTRecord = NTRecordC {getNTRecordC :: Word}

data ADTRecord = ADTRecordA {adtA :: Int}
               | ADTRecordB {adtB :: Bool, adtBB :: String}

data Reversed a b = RL b | RR a

