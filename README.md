# h2ps-types

Example:

```haskell
-- translate/Main.hs
module Main where

import Prelude
import Type.HS2PS (renderPSTypes)

main :: IO ()
main = do
  let ts :: String
      ts = $(renderPSTypes
        [ ''Maybe
        , ''Either
        ] )
  let moduleName = "ImportedTypes"
  writeFile ("purs/src/" <> moduleName <> ".purs") $
    "module " <> moduleName <> " where\n\n" <> ts
```

```purescript
module ImportedTypes where

data Maybe a = Nothing | Just a
data Either a b = Left a | Right b
```

```yaml
# package.yaml
executables:
  translate-types:
    main: Main.hs
    source-dirs: translate
    ghc-options:
    - -threaded
    - -rtsopts
    - -with-rtsopts=-N
    dependencies:
    - <package-name>
    - hs2ps
    when:
    - condition: flag(library-only)
      buildable: false
```

